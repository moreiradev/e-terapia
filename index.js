/**
 * @format
 */

import { AppRegistry } from "react-native";
import App from "./src/App";
import { name as appName } from "./app.json";
import HorizontalCarousel from "./src/componentes/HorizontalScroll";

AppRegistry.registerComponent(appName, () => App);
