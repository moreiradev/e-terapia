import { React } from 'React';
import { StyleSheet, Dimensions } from 'react-native';

const larguraDaTela = Dimensions.get('window').width;
const alturaDaTela = Dimensions.get('window').height;

const styles = StyleSheet.create({
  containeFormLogin: {
    flex: 1,
    alignItems: 'center',
    top: 50,
    position: 'absolute'
  },
  tituloLogin: {
    position: 'relative',
    marginTop: 20,
    fontSize: 24,
    color: 'rgba(21, 25, 91, 1)'
  },
  textoLogin: {
    position: 'relative',
    marginTop: 15,
    color: 'rgba(77, 77, 77, 0.8)',
    fontSize: 12
  },
  linkIsPsicologoCadastro: {
    fontFamily: 'Poppins Regular',
    marginTop: 20,
    color: '#3CC0CE',
    textDecorationLine: 'underline',
    fontSize: 12
  },
  containerTuto: {
    height: alturaDaTela,
    width: larguraDaTela,
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'visible'
  },
  tituloTutorial: {
    marginBottom: 10,
    /*  top: -11, */
    position: 'relative',
    fontFamily: 'Poppins Regular',
    fontSize: 21,
    padding: 15,
    color: '#15195B',
    textAlign: 'center'
  },
  textoTuto: {
    padding: 50,
    position: 'relative',
    fontFamily: 'Poppins Regular',
    fontSize: 12,
    top: -70,
    color: '#AFC1C4',
    textAlign: 'center'
  },
  imageTuto: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    top: -95,
    overflow: 'visible'
  },
  logoHome: {
    top: -20,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    overflow: 'visible'
  },
  formLoginInput: {
    marginTop: 27,
    position: 'relative',
    top: -50,
    textAlign: 'center',
    height: 60,
    borderColor: 'rgba(0, 0, 0, 0.05)',
    backgroundColor: '#FFFFFF',
    width: 330,
    borderWidth: 1,
    borderStyle: 'solid',
    fontSize: 15,
    borderRadius: 25,
    shadowColor: 'rgba(0, 0, 0, 1)',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1.0
  },
  formCadastroInput: {
    color: '#999999',
    marginTop: 10,
    position: 'relative',
    top: -50,
    textAlign: 'center',
    height: 60,
    borderColor: 'rgba(0, 0, 0, 0.05)',
    backgroundColor: '#FFFFFF',
    width: 330,
    borderWidth: 1,
    borderStyle: 'solid',
    fontSize: 15,
    borderRadius: 25,
    shadowColor: 'rgba(0, 0, 0, 1)',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1.0
  },
  styleCheckCadastro: {
    color: '#999999',
    marginTop: 22,
    position: 'relative',
    top: -50,
    textAlign: 'center',
    height: 60,
    width: 330,
    fontSize: 15
  },
  formLoginContainer: {
    marginTop: 80,
    position: 'relative'
  },
  formCadastroContainer: {
    position: 'relative',
    marginTop: 207
  },
  actionLogin: {
    marginTop: -18,
    position: 'relative'
  },
  actionCadastro: {
    marginTop: -20,
    position: 'relative'
  },
  isNovoAqui: {
    position: 'absolute',
    bottom: 45,
    fontSize: 14,
    fontFamily: 'Poppins Regular'
  },
  jaPossuiCadastro: {
    position: 'absolute',
    bottom: 15,
    fontSize: 14,
    fontFamily: 'Poppins Regular'
  },
  textLoginDefault: {
    fontSize: 14,
    fontFamily: 'Poppins Regular',
    color: '#15195B'
  },
  textLoginBold: {
    fontSize: 14,
    fontFamily: 'Poppins Regular',
    color: '#15195B',
    fontWeight: 'bold'
  }
});

module.exports = styles;
