import React from 'react';
import { View, StyleSheet } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import HorizontalCarousel from './componentes/HorizontalScroll';
import Login from './componentes/Login';
import Cadastro from './componentes/Cadastro';

// eslint-disable-next-line react/prefer-stateless-function
class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        {/* <HorizontalCarousel /> */}
        <Login />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: HorizontalCarousel,
      navigationOptions: {
        header: null
      }
    },
    Login: {
      screen: Login,
      navigationOptions: {
        header: null
      }
    },
    Cadastro: {
      screen: Cadastro,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: 'Home'
  }
);

export default createAppContainer(AppNavigator);
