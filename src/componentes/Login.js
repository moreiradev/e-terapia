import React from 'react';
import {
  ScrollView,
  Text,
  View,
  Image,
  ImageBackground,
  TextInput
} from 'react-native';
import { Button } from 'react-native-elements';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import { Input } from 'react-native-elements';
import styles from '../estilos/Global';

// eslint-disable-next-line react/prefer-stateless-function
export default class Login extends React.Component {
  render() {
    return (
      <View style={styles.containerTuto}>
        <ImageBackground
          style={styles.containerTuto}
          // eslint-disable-next-line global-require
          source={require('../../assets/img/bg-home-ap.png')}
        >
          <View style={styles.containeFormLogin}>
            <Image source={require('../../assets/img/logo-brain.png')} />
            <Text style={styles.tituloLogin}>Bem-vindo!</Text>
            <Text style={styles.textoLogin}>Faça login na sua conta.</Text>
          </View>
          <View style={styles.formCadastroContainer}>
            <TextInput
              keyboardType="email-address"
              style={styles.formLoginInput}
              placeholder="E-mail"
              leftIcon={{ type: 'font-awesome', name: 'chevron-left' }}
            />
            <TextInput
              // eslint-disable-next-line react/jsx-boolean-value
              secureTextEntry={true}
              style={styles.formLoginInput}
              placeholder="Senha"
              leftIcon={{ type: 'font-awesome', name: 'chevron-left' }}
            />
          </View>
          <View style={styles.actionLogin}>
            <Button
              buttonStyle={{
                backgroundColor: '#3CC0CE',
                fontFamily: 'Poppins Regular',
                width: 330,
                top: -10,
                position: 'relative',
                height: 52,
                borderRadius: 20,
                shadowColor: 'rgba(236, 242, 254, 1)',
                shadowOpacity: 0.8,
                elevation: 6,
                shadowRadius: 15,
                shadowOffset: {
                  width: 1,
                  height: 13
                }
              }}
              title="Entrar"
            />
          </View>
          <View style={styles.isNovoAqui}>
            <Text>
              <Text style={styles.textLoginDefault}>É novo aqui?</Text>
              <Text
                onPress={() => this.props.navigation.navigate('Cadastro')}
                style={styles.textLoginBold}
              >
                {' '}
                Criar uma conta
              </Text>
            </Text>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
