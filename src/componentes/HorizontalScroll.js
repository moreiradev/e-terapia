import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { ScrollView, Text, View, Image, ImageBackground } from 'react-native';
import { Button } from 'react-native-elements';
import styles from '../estilos/Global';

// eslint-disable-next-line react/prefer-stateless-function
export default class HorizontalCarousel extends React.Component {
  render() {
    return (
      <ScrollView
        // eslint-disable-next-line react/jsx-boolean-value
        horizontal={true}
        // eslint-disable-next-line react/jsx-boolean-value
        pagingEnabled={true}
        showsHorizontalScrollIndicator={false}
        onMomentumScrollEnd={() => {}}
        scrollEventThrottle={10}
      >
        <View style={styles.containerTuto}>
          <ImageBackground
            style={styles.containerTuto}
            // eslint-disable-next-line global-require
            source={require('../../assets/img/bg-home-ap.png')}
          >
            <Image
              style={styles.logoHome}
              // eslint-disable-next-line global-require
              source={require('../../assets/img/logo-e-terapia.png')}
            />
          </ImageBackground>
        </View>
        <View style={styles.containerTuto}>
          <ImageBackground
            style={styles.containerTuto}
            // eslint-disable-next-line global-require
            source={require('../../assets/img/bg-tuto.png')}
          >
            <Text style={styles.tituloTutorial}>Qualidade no atendimento</Text>
            <Text style={styles.textoTuto}>
              Seja atendido online por profissionais selecionados, com ampla
              experiência clínica.
            </Text>
            <Image
              style={styles.imageTuto}
              // eslint-disable-next-line global-require
              source={require('../../assets/img/step-one-tuto.png')}
            />
          </ImageBackground>
        </View>
        <View style={styles.containerTuto}>
          <ImageBackground
            style={styles.containerTuto}
            // eslint-disable-next-line global-require
            source={require('../../assets/img/bg-tuto.png')}
          >
            <Text style={styles.tituloTutorial}>
              Terapia ao alcance das mãos
            </Text>
            <Text style={styles.textoTuto}>
              No conforto da sua casa, em uma sala de reunião ou durante o
              horário de almoço. Tenha o seu psicólogo ao alcance das mãos!
            </Text>
            <Image
              style={styles.imageTuto}
              // eslint-disable-next-line global-require
              source={require('../../assets/img/step-two-tuto.png')}
            />
          </ImageBackground>
        </View>
        <View style={styles.containerTuto}>
          <ImageBackground
            style={styles.containerTuto}
            // eslint-disable-next-line global-require
            source={require('../../assets/img/bg-tuto.png')}
          >
            <Text style={styles.tituloTutorial}>Seu tempo vale ouro</Text>
            <Text style={styles.textoTuto}>
              O e-terapia vem revolucionar o atendimento psicológico para você
              que não tem tempo de se deslocar até uma clínica.
            </Text>
            <Image
              style={styles.imageTuto}
              // eslint-disable-next-line global-require
              source={require('../../assets/img/step-3-tuto.png')}
            />
          </ImageBackground>
        </View>
        <View style={styles.containerTuto}>
          <ImageBackground
            style={styles.containerTuto}
            // eslint-disable-next-line global-require
            source={require('../../assets/img/bg-tuto.png')}
          >
            <Text style={styles.tituloTutorial}>
              Segurança em primeiro lugar
            </Text>
            <Text style={styles.textoTuto}>
              Sinta-se em um consultório. Todos os diálogos entre você e o seu
              psicologo não são armazenados e ficam entre vocês.
            </Text>
            <Image
              style={styles.imageTuto}
              // eslint-disable-next-line global-require
              source={require('../../assets/img/step-4-tuto.png')}
            />
            <Button
              // eslint-disable-next-line react/destructuring-assignment
              onPress={() => this.props.navigation.navigate('Login')}
              buttonStyle={{
                backgroundColor: '#3CC0CE',
                fontFamily: 'Poppins Regular',
                width: 295,
                position: 'relative',
                top: -45,
                height: 59,
                borderRadius: 5,
                shadowColor: 'rgba(236, 242, 254, 1)',
                shadowOpacity: 0.8,
                elevation: 6,
                shadowRadius: 15,
                shadowOffset: {
                  width: 1,
                  height: 13
                }
              }}
              title="Vamos começar"
            />
          </ImageBackground>
        </View>
      </ScrollView>
    );
  }
}
