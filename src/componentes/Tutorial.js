import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";

const styles = StyleSheet.create({
  slide: {},
  title: {},
  text: {},
  image: {
    position: "absolute",
    bottom: 0
  }
});

const slides = [
  {
    key: "somethun",
    title: "Title 1",
    text: "Description.\nSay something cool",
    image: require("../../assets/img/step-one-tuto.png"),
    imageStyle: styles.image,
    backgroundColor: "#59b2ab"
  },
  {
    key: "somethun-dos",
    title: "Title 2",
    text: "Other cool stuff",
    image: require("../../assets/img/step-one-tuto.png"),
    imageStyle: styles.image,
    backgroundColor: "#febe29"
  },
  {
    key: "somethun1",
    title: "Rocket guy",
    text: "I'm already out of descriptions\n\nLorem ipsum bla bla bla",
    image: require("../../assets/img/step-one-tuto.png"),
    imageStyle: styles.image,
    backgroundColor: "#22bcb5"
  }
];
export default class Tutorial extends React.Component {
  _onDone = () => {
    // User finished the introduction. Show "real" app
  };
  _renderItem = ({ item, dimensions }) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.text}>{item.text}</Text>
        <Image source={item.image} />
      </View>
    );
  };
  render() {
    return <AppIntroSlider slides={slides} onDone={this._onDone} />;
  }
}
