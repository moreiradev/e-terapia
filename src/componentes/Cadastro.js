import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import {
  ScrollView,
  Text,
  View,
  Image,
  ImageBackground,
  TextInput,
  CheckBox
} from 'react-native';
import { Button } from 'react-native-elements';
import styles from '../estilos/Global';

// eslint-disable-next-line react/prefer-stateless-function
export default class Cadastro extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // eslint-disable-next-line react/no-unused-state
      checked: false
    };
  }

  onChangeCheck() {
    this.setState({ checked: !this.state.checked });
  }

  render() {
    return (
      <View style={styles.containerTuto}>
        <ImageBackground
          style={styles.containerTuto}
          // eslint-disable-next-line global-require
          source={require('../../assets/img/bg-home-ap.png')}
        >
          <View style={styles.containeFormLogin}>
            <Image source={require('../../assets/img/logo-brain.png')} />
            <Text style={styles.tituloLogin}>Vamos começar?</Text>
            <Text style={styles.textoLogin}>
              Precisamos de algumas informações para criar a sua conta.
            </Text>
            <Text style={styles.linkIsPsicologoCadastro}>
              Você é psicólogo?
            </Text>
          </View>
          <View style={styles.formCadastroContainer}>
            <TextInput
              keyboardType="default"
              style={styles.formCadastroInput}
              placeholder="Seu Nome"
            />
            <TextInput
              keyboardType="email-address"
              style={styles.formCadastroInput}
              placeholder="E-mail"
            />
            <TextInput
              // eslint-disable-next-line react/jsx-boolean-value
              secureTextEntry={true}
              style={styles.formCadastroInput}
              placeholder="Senha"
            />
            <View style={styles.styleCheckCadastro}>
              <CheckBox
                label="FEMALE"
                style={styles.checkBox}
                value={this.state.checked}
                onChange={() => this.onChangeCheck()}
              />
              <Text style={styles.styleCheckCadastro}>
                Tenho 18 anos ou mais
              </Text>
            </View>
          </View>
          <View style={styles.actionCadastro}>
            <Button
              buttonStyle={{
                backgroundColor: '#3CC0CE',
                fontFamily: 'Poppins Regular',
                width: 330,
                top: -10,
                position: 'relative',
                height: 52,
                borderRadius: 20,
                shadowColor: 'rgba(236, 242, 254, 1)',
                shadowOpacity: 0.8,
                elevation: 6,
                shadowRadius: 15,
                shadowOffset: {
                  width: 1,
                  height: 13
                }
              }}
              title="Começar"
            />
          </View>
          <View style={styles.jaPossuiCadastro}>
            <Text>
              <Text style={styles.textLoginDefault}>Já tem uma conta?</Text>
              <Text
                // eslint-disable-next-line react/destructuring-assignment
                onPress={() => this.props.navigation.navigate('Login')}
                style={styles.textLoginBold}
              >
                {' '}
                Entre agora
              </Text>
            </Text>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
